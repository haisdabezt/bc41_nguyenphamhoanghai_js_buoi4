// bài 1
const sapXep = () => {
  var so1 = document.getElementById("so1").value * 1;
  var so2 = document.getElementById("so2").value * 1;
  var so3 = document.getElementById("so3").value * 1;
  var sausapxep = "";

  if (so1 <= so2 && so2 <= so3) {
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so1} , ${so2} , ${so3}`;
  } else if (so1 <= so3 && so3 <= so2)
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so1} , ${so3} , ${so2}`;
  else if (so2 <= so1 && so1 <= so3) {
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so2} , ${so1} , ${so3}`;
  } else if (so2 <= so3 && so3 <= so1) {
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so2} , ${so3} , ${so1}`;
  } else if (so3 <= so1 && so1 <= so2) {
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so3} , ${so1} , ${so2}`;
  } else {
    document.getElementById(
      "sausapxep"
    ).innerHTML = ` ${so3} , ${so2} , ${so1}`;
  }
};

// bài 2
const ketQua = () => {
  var giatri = document.getElementById("danhsach").value;
  switch (giatri) {
    case "CTV":
      {
        document.getElementById(
          "thanhvien"
        ).innerHTML = ` Xin chào bạn gì đó ơi`;
      }
      break;

    case "B":
      {
        document.getElementById("thanhvien").innerHTML = ` Xin chào Bố`;
      }
      break;

    case "M":
      {
        document.getElementById("thanhvien").innerHTML = ` Xin chào Mẹ`;
      }
      break;

    case "AT":
      {
        document.getElementById("thanhvien").innerHTML = ` Xin chào Anh Trai`;
      }
      break;

    case "EG":
      {
        document.getElementById("thanhvien").innerHTML = ` Xin chào Em Gái`;
      }
      break;
  }
};
// bài 3
const xuatChanLe = () => {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  if ((num1, num2, num3 > 0)) {
    if (num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 == 0) {
      document.getElementById("xuatchanle").innerHTML = ` có 3 số chẵn `;
    } else if (
      (num1 % 2 == 0 && num2 % 2 == 0) ||
      (num1 % 2 == 0 && num3 % 2 == 0) ||
      (num2 % 2 == 0) & (num3 % 2 == 0)
    ) {
      document.getElementById(
        "xuatchanle"
      ).innerHTML = ` có 2 số chẵn , 1 số lẻ `;
    } else if (num1 % 2 == 0 || num2 % 2 == 0 || num3 % 2 == 0) {
      document.getElementById(
        "xuatchanle"
      ).innerHTML = ` có 1 số chẵn , 2 số lẻ `;
    } else {
      document.getElementById("xuatchanle").innerHTML = ` có 3 số lẻ`;
    }
  } else {
    alert("Vui lòng nhập số nguyên dương");
  }
};

// bài 4
const tamGiac = () => {
  var c1 = document.getElementById("c1").value * 1;
  var c2 = document.getElementById("c2").value * 1;
  var c3 = document.getElementById("c3").value * 1;

  if (c1 > 0 && c2 > 0 && c3 > 0) {
    if (c1 + c2 > c3 && c1 + c3 > c2 && c2 + c3 > c1) {
      if (c1 == c2 && c1 == c3) {
        document.getElementById("tamgiac").innerHTML = ` Đây là tam giác đều `;
      } else if (c1 == c2 || c1 == c3 || c2 == c3) {
        document.getElementById("tamgiac").innerHTML = ` Đây là tam giác cân `;
      } else if (
        c1 * c1 == c2 * c2 + c3 * c3 ||
        c2 * c2 == c1 * c1 + c3 * c3 ||
        c3 * c3 == c2 * c2 + c1 * c1
      ) {
        document.getElementById(
          "tamgiac"
        ).innerHTML = ` Đây là tam giác vuông `;
      }
    } else {
      alert("Đây không phải là hình tam giác");
    }
  } else {
    alert("Vui lòng nhập các cạnh là số nguyên dương");
  }
};
